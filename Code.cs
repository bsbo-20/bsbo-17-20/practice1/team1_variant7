﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace Modeling
{
	public class Program
	{
		static void Main()
		{
			var cntr = new TimeTrackingController();

			Console.WriteLine("-----------------------------------------");
			Console.WriteLine("Прецедент: Создание и назначение задач\n");
			cntr.GetUsers();
			cntr.CreateTask("data");
			Console.WriteLine("-----------------------------------------");

			Console.WriteLine("-----------------------------------------");
			Console.WriteLine("Прецедент: Выполнение задачи\n");
			cntr.GetTasks(1);
			cntr.ChangeTaskStatus(1, "going");
			cntr.ChangeTaskStatus(1, "finished");
			Console.WriteLine("-----------------------------------------");

			Console.WriteLine("-----------------------------------------");
			Console.WriteLine("Прецедент: Отслеживание прогресса задач\n");
			cntr.ViewProgress();
			Console.WriteLine("-----------------------------------------");

			Console.WriteLine("-----------------------------------------");
			Console.WriteLine("Прецедент: Расчет производительности\n");
			cntr.ComputePerformance("period");
			Console.WriteLine("-----------------------------------------");
		}
	}

	public class TimeTrackingController
	{
		private readonly UserList _userList = new();
		private readonly TaskList _taskList = new();
		private readonly PerformanceComputer _performanceComputer = new();

		public void CreateTask(string data)
		{
			Console.WriteLine(
				$"У {nameof(TimeTrackingController)} вызван метод {nameof(TimeTrackingController.CreateTask)}");

			var task = new Task();

			_taskList.AddTask(task);
		}

		public string ComputePerformance(string period)
		{
			Console.WriteLine(
				$"У {nameof(TimeTrackingController)} вызван метод {nameof(TimeTrackingController.ComputePerformance)}");

			List<User> users = _userList.GetAll();
			List<Task> tasks = _taskList.GetAll();

			return _performanceComputer.Compute(users, tasks);
		}

		public string GetTasks(int userID)
		{
			Console.WriteLine(
				$"У {nameof(TimeTrackingController)} вызван метод {nameof(TimeTrackingController.GetTasks)}");

			List<Task> tasks = _taskList.GetAll().Where(t => t.AuthorID == userID).ToList();

			return "result";
		}

		public void ChangeTaskStatus(int taskID, string status)
		{
			Console.WriteLine(
				$"У {nameof(TimeTrackingController)} вызван метод {nameof(TimeTrackingController.ChangeTaskStatus)}");

			_taskList.ChangeTaskStatus(taskID, status);
		}

		public string GetUsers()
		{
			Console.WriteLine(
				$"У {nameof(TimeTrackingController)} вызван метод {nameof(TimeTrackingController.GetUsers)}");

			List<User> users = _userList.GetAll();

			return "result";
		}

		public string ViewProgress()
		{
			Console.WriteLine(
				$"У {nameof(TimeTrackingController)} вызван метод {nameof(TimeTrackingController.ViewProgress)}");

			List<Task> tasks = _taskList.GetAll();

			return "result";
		}
	}

	public class TaskList
	{
		private readonly List<Task> _tasks = new();

		public void AddTask(Task task)
		{
			Console.WriteLine(
				$"У {nameof(TaskList)} вызван метод {nameof(TaskList.AddTask)}");
		}

		public void RemoveTask(int id)
		{
			Console.WriteLine(
				$"У {nameof(TaskList)} вызван метод {nameof(TaskList.RemoveTask)}");
		}

		public List<Task> GetAll()
		{
			Console.WriteLine(
				$"У {nameof(TaskList)} вызван метод {nameof(TaskList.GetAll)}");
			return _tasks;
		}

		public Task GetTask(int id)
		{
			Console.WriteLine(
				$"У {nameof(TaskList)} вызван метод {nameof(TaskList.GetTask)}");

			return _tasks.FirstOrDefault(t => t.Id == id);
		}

		public void ChangeTaskStatus(int id, string status)
		{
			var task = _tasks.FirstOrDefault(t => t.Id == id);
			if (task != null)
				task.Status = status;
			Console.WriteLine(
				$"У {nameof(Task)} изменено свойство {nameof(Task.Status)}");
			Console.WriteLine(
				$"У {nameof(TaskList)} вызван метод {nameof(TaskList.ChangeTaskStatus)}");
		}
	}

	public class UserList
	{
		private readonly List<User> _users = new();

		public void AddUser(User user)
		{
			Console.WriteLine(
				$"У {nameof(UserList)} вызван метод {nameof(UserList.AddUser)}");
		}

		public void RemoveUser(int id)
		{
			Console.WriteLine(
				$"У {nameof(UserList)} вызван метод {nameof(UserList.RemoveUser)}");
		}

		public List<User> GetAll()
		{
			Console.WriteLine(
				$"У {nameof(UserList)} вызван метод {nameof(UserList.GetAll)}");

			return _users;
		}

		public User GetUser(int id)
		{
			Console.WriteLine(
				$"У {nameof(UserList)} вызван метод {nameof(UserList.GetUser)}");

			return _users.FirstOrDefault(u => u.Id == id);
		}
	}

	public class PerformanceComputer
	{
		public string Compute(List<User> users, List<Task> tasks)
		{
			Console.WriteLine(
				$"У {nameof(PerformanceComputer)} вызван метод {nameof(PerformanceComputer.Compute)}");

			var data = new DataTable();
			foreach (var user in users)
			{
				var filteredTasks = tasks.FindAll(t => t.Users.Contains(user));
				foreach (var task in filteredTasks)
				{
					//abracadabra
				}
			}

			return GenerateReport(data);
		}

		private string GenerateReport(DataTable data)
		{
			Console.WriteLine(
				$"У {nameof(PerformanceComputer)} вызван метод {nameof(PerformanceComputer.GenerateReport)}");

			return "result";
		}
	}

	public class Task
	{
		public int Id { get; set; }
		public int AuthorID { get; set; }
		public string Title { get; set; }
		public DateTime CreationTime { get; set; }
		public DateTime DeadlineTime { get; set; }
		public int Priority { get; set; }
		public string Status { get; set; }
		public List<User> Users { get; set; }

		public Task()
		{
			Console.WriteLine($"У {nameof(Task)} вызван конструктор.");
		}
	}

	public class User
	{
		public int Id { get; set; }
		public string Role { get; set; }
		public string Name { get; set; }
		public string Contacts { get; set; }
	}
}